import {Timer} from "./Timer";


let minutes: HTMLInputElement = <HTMLInputElement>document.getElementById('minutes');
let seconds: HTMLInputElement = <HTMLInputElement>document.getElementById('seconds');

let submitButton: HTMLInputElement = <HTMLInputElement>document.getElementById('start');
submitButton.onclick = function(){
	// Get the input values.
	let numMinutes: number = parseInt(minutes.value);
	let numSeconds: number = parseInt(seconds.value);

	// Construct a new timer from the input values.
	let timer: Timer = new Timer(numMinutes, numSeconds, function(){
		minutes.value = String(timer.minutes);
		seconds.value = String(timer.seconds);
	});

	timer.start();
	return false;
}