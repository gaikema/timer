export class Timer
{
	public minutes: number;
	public seconds: number;
	
	private intervalObject;
	// Function to call every second.
	private callback: () => void;

	constructor(minutes: number, seconds: number, callback = null)
	{
		this.minutes = minutes;
		this.seconds = seconds;

		this.callback = callback;
	}

	// Return type any to avoid errors.
	public start(): any
	{
		let t: number = this.minutes*60 + this.seconds;
		// Important for complicated reasons.
		let _this = this;
		// Reset old interval.
		clearInterval(this.intervalObject);
		this.intervalObject = setInterval(function(){
			_this.minutes = Math.floor(t / 60);
			_this.seconds = Math.floor(t % 60);

			console.log(_this.minutes + " minutes and " + _this.seconds + " seconds.");
			_this.callback();

			t--;
		}, 1000);
	}

	public stop(): void
	{
		clearInterval(this.intervalObject);
	}
}